# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "us-east-1"
}

# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = ["22", "8080", "80"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web access for Application"
  }
}

#Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, ,

resource "aws_launch_configuration" "web" {
  name_prefix     = "Docker-"
  # какой будет использоваться образ
  image_id        = data.aws_ami.ubuntu.id
  # Размер машины (CPU и память)
  instance_type   = "t2.micro"
  # какие права доступа
  security_groups = [aws_security_group.web.id]
  # какие следует запустить скрипты при создании сервера
  user_data       = file("user_data.sh")
  # какой SSH ключ будет использоваться
  key_name = "MacBook"
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}

# AWS Autoscaling Group для указания, сколько нам понадобится инстансов
resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 1
  max_size             = 1
  min_elb_capacity     = 1
  wait_for_capacity_timeout = 0
  health_check_type    = "ELB"
  # и в каких подсетях, каких Дата центрах их следует разместить
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  # Ссылка на балансировщик нагрузки, который следует использовать
  load_balancers       = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name   = "Docker"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}
#Создаем Load Balancer Classic, который проксирует трафик к нашим иснтансам
resource "aws_elb" "web" {
  name               = "NGINX-Highly-Available-ELB"
  # перенаправляет трафик на несколько Дата центров
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  # слушает на порту 8080
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  tags = {
    Name = "Docker-ELB"
  }
}

#resource "aws_elb_attachment" "test" {
#  elb      = aws_elb.web.id
#  instance = aws_instance.my_webserver.id
#}
#

# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_route53_zone" "public" {
  name = "zhupinskiy.click"

  tags = {
    Environment = "dev"
  }
}

resource "aws_route53_record" "task1" {
  zone_id = aws_route53_zone.public.zone_id
  name    = "ELB"
  type    = "NS"
  ttl     = "300"
  records = [aws_elb.web.dns_name]
}

# Выведем в консоль DNS имя нашего сервера
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}


